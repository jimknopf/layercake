/*
 * Copyright 2018 Michael Knopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.jimknopf.layercake;

import org.bitbucket.jimknopf.layercake.math.*;

import static java.lang.Math.*;

/**
 * Blueprint for triangular meshes with {@value SUBDIVISIONS} subdivisions.
 * The blueprint is both a quad-tree and a quad-heap: all cells maintain
 * references to their parent and child cells, and cells are stored level by
 * level in a single array to allow for iterating efficiently over a single
 * level (no traversal of the tree required).
 *
 * <p>To work with the blueprint as quad-tree, the method {@link #root()}
 * allows to look up the root node.
 *
 * <p>To work with the blueprint as heap, the methods {@link #cell(int)} and
 * {@link #cellCount(int)} can be used. The former allows to look up a cell
 * by its index. The latter returns the size of a heap with the given
 * subdivisions and thus the index of the first cell at the next subdivision
 * level. See the implementation of {@link #buildHeap()} for an example.
 */
public class Blueprint {

    public static final int SUBDIVISIONS = 5;
    public static final int EDGE_LENGTH = (1 << SUBDIVISIONS) + 1;
    public static final int N_COORDS = EDGE_LENGTH * (EDGE_LENGTH + 1) / 2;
    public static final int N_CELLS = cellCount(SUBDIVISIONS);
    public static final float HEIGHT = (float) (sqrt(3d) / 2d);

    private static Blueprint instance;

    /**
     * Orientation of a triangle/cell.
     */
    enum Orientation {
        MINUS_Z_FACING,
        Z_FACING
    }

    /**
     * A single cell of the blueprint. Has a unique id and unique vertex ids,
     * as well as, references to it parent and sibling cells.
     */
    public static class Cell {

        public static final int UNDEFINED = -1;

        final int id;
        final Orientation orientation;

        Cell parent;
        Cell childX, childY, childZ, childC;
        Cell siblingXY, siblingYZ, siblingZX;
        int vertexX = UNDEFINED, vertexY = UNDEFINED, vertexZ = UNDEFINED;

        Cell(Cell parent, int id, Orientation orientation) {
            this.parent = parent;
            this.id = id;
            this.orientation = orientation;
        }

        @Override
        public String toString() {
            return "(" + vertexX + ", " + vertexY + ", " + vertexZ + ")";
        }

    }

    private final Vec3fList vertices = new Vec3fList(N_COORDS, true);
    private final Vec3fList weights = new Vec3fList(N_COORDS);
    private final Cell[] heap = new Cell[N_CELLS];

    private Blueprint() {
        // Build tree/heap structure
        buildHeap();
        connectSibling();
        // Determine vertex ids
        assignVertexIds();
        // Determine vertex positions and weights
        initPositions();
        initWeights();
    }

    public Vec3fList positions() {
        return vertices;
    }

    public Vec3fList weights() {
        return weights;
    }

    public Cell root() {
        return heap[0];
    }

    public Cell cell(int i) {
        return heap[i];
    }

    /**
     * Build the quad-tree (heap) top down: initialize all cells and parents
     * with their child cells.
     */
    private void buildHeap() {
        heap[0] = new Cell(null, 0, Orientation.Z_FACING);
        int cellId = 1;
        for (int level = 0; level < SUBDIVISIONS; level++) {
            int firstParent = cellCount(level - 1);
            int firstSibling = cellCount(level);
            for (int pid = firstParent; pid < firstSibling; pid++) {
                Cell parent = heap[pid];
                Orientation aligned = parent.orientation;
                Orientation opposed = aligned == Orientation.Z_FACING
                        ? Orientation.MINUS_Z_FACING : Orientation.Z_FACING;
                parent.childX = new Cell(parent, cellId++, aligned);
                parent.childY = new Cell(parent, cellId++, aligned);
                parent.childZ = new Cell(parent, cellId++, aligned);
                parent.childC = new Cell(parent, cellId++, opposed);
                heap[parent.childX.id] = parent.childX;
                heap[parent.childY.id] = parent.childY;
                heap[parent.childZ.id] = parent.childZ;
                heap[parent.childC.id] = parent.childC;
            }
        }
    }

    /**
     * Connects sibling cells with each other (cells on the same level).
     */
    private void connectSibling() {
        for (int level = 0; level < SUBDIVISIONS; level++) {
            int firstParent = cellCount(level - 1);
            int firstSibling = cellCount(level);
            for (int pid = firstParent; pid < firstSibling; pid++) {
                // Sibling connections within the parent cell
                Cell parent = heap[pid];
                parent.childX.siblingYZ = parent.childC;
                parent.childC.siblingXY = parent.childZ;
                parent.childY.siblingZX = parent.childC;
                parent.childC.siblingYZ = parent.childX;
                parent.childZ.siblingXY = parent.childC;
                parent.childC.siblingZX = parent.childY;

                // Sibling connections to children of neighboring cells
                if (parent.siblingXY != null) {
                    parent.childX.siblingXY = parent.siblingXY.childY;
                    parent.childY.siblingXY = parent.siblingXY.childX;
                }
                if (parent.siblingYZ != null) {
                    parent.childY.siblingYZ = parent.siblingYZ.childZ;
                    parent.childZ.siblingYZ = parent.siblingYZ.childY;
                }
                if (parent.siblingZX != null) {
                    parent.childZ.siblingZX = parent.siblingZX.childX;
                    parent.childX.siblingZX = parent.siblingZX.childZ;
                }
            }
        }
    }

    /**
     * Assigns the vertex indices top-down.
     */
    private void assignVertexIds() {
        int vertexCounter = 0;
        Cell root = heap[0];
        root.vertexX = vertexCounter++;
        root.vertexY = vertexCounter++;
        root.vertexZ = vertexCounter++;

        for (int level = 0; level < SUBDIVISIONS; level++) {
            int firstParent = cellCount(level - 1);
            int firstSibling = cellCount(level);
            for (int pid = firstParent; pid < firstSibling; pid++) {
                // Look up midpoint vertex ids from siblings, generate new if
                // missing or undefined
                Cell parent = heap[pid];
                int xy = Cell.UNDEFINED;
                int yz = Cell.UNDEFINED;
                int zx = Cell.UNDEFINED;

                if (parent.siblingXY != null) {
                    xy = parent.siblingXY.childC.vertexZ;
                }
                if (xy == Cell.UNDEFINED) {
                    xy = vertexCounter++;
                }

                if (parent.siblingYZ != null) {
                    yz = parent.siblingYZ.childC.vertexX;
                }
                if (yz == Cell.UNDEFINED) {
                    yz = vertexCounter++;
                }

                if (parent.siblingZX != null) {
                    zx = parent.siblingZX.childC.vertexY;
                }
                if (zx == Cell.UNDEFINED) {
                    zx = vertexCounter++;
                }

                // Set vertex ids of child cells
                parent.childX.vertexX = parent.vertexX;
                parent.childX.vertexY = xy;
                parent.childX.vertexZ = zx;

                parent.childY.vertexX = xy;
                parent.childY.vertexY = parent.vertexY;
                parent.childY.vertexZ = yz;

                parent.childZ.vertexX = zx;
                parent.childZ.vertexY = yz;
                parent.childZ.vertexZ = parent.vertexZ;

                parent.childC.vertexX = yz;
                parent.childC.vertexY = zx;
                parent.childC.vertexZ = xy;
            }
        }
    }

    /**
     * Initializes the positions of the vertices. The top level is always a
     * equilateral triangle with side length one centered around the origin.
     * All other vertex positions are derive top-down.
     */
    private void initPositions() {
        // Initialize vertices of root cell depending on orientation
        Cell root = heap[0];
        vertices.set(root.vertexX, -0.5f, 0f, -HEIGHT / 3f);
        vertices.set(root.vertexY, 0f, 0f, 2f * HEIGHT / 3f);
        vertices.set(root.vertexZ, 0.5f, 0f, -HEIGHT / 3f);

        // Derive remaining positions and texture coordinates top-down
        int firstLeaf = cellCount(SUBDIVISIONS - 1);
        Vec3f a = new Vec3f(), b = new Vec3f();
        for (int i = 0; i < firstLeaf; i ++) {
            Cell cell = heap[i];
            // Midpoint between X and Y
            vertices.get(cell.vertexX, a);
            vertices.get(cell.vertexY, b);
            b.sub(a).scl(0.5f).add(a);
            vertices.set(cell.childC.vertexZ, b);

            // Midpoint between Y and Z
            vertices.get(cell.vertexY, a);
            vertices.get(cell.vertexZ, b);
            b.sub(a).scl(0.5f).add(a);
            vertices.set(cell.childC.vertexX, b);

            // Midpoint between Z and X
            vertices.get(cell.vertexZ, a);
            vertices.get(cell.vertexX, b);
            b.sub(a).scl(0.5f).add(a);
            vertices.set(cell.childC.vertexY, b);
        }
    }

    /**
     * Computes the influence weights of the three top level vertices for all
     * vertices of the blueprint.
     */
    private void initWeights() {
        Cell root = heap[0];
        Vec3f x = vertices.get(root.vertexX, new Vec3f());
        Vec3f y = vertices.get(root.vertexY, new Vec3f());
        Vec3f z = vertices.get(root.vertexZ, new Vec3f());
        Vec3f vertex = new Vec3f();
        for (int i = 0; i < N_COORDS; i++) {
            vertices.get(i, vertex);
            weights.add(weights(vertex, x, y, z));
        }
    }

    /**
     * Computes the influence weights of vertices A, B, and C for the given
     * point P. Assumes that the triangle (A, B, C) is equilateral and
     * encloses P. The weights are normalized to always add up to one.
     *
     * @param p the point to compute weights for
     * @param a vertex A of the enclosing triangle
     * @param b vertex B of the enclosing triangle
     * @param c vertex C of the enclosing triangle
     * @return the influence weights for P
     */
    private static Vec3f weights(Vec3f p, Vec3f a, Vec3f b, Vec3f c) {
        // Euclidean distances to A, B, and C.
        Vec3f tmp = new Vec3f();
        Vec3f weights = new Vec3f(
                tmp.set(a).sub(p).len(),
                tmp.set(b).sub(p).len(),
                tmp.set(c).sub(p).len()
        );
        // Assume equilateral triangle with edge length |B-A|.
        float edgeLength = tmp.set(b).sub(a).len();
        weights.scl(1f / (edgeLength * HEIGHT));
        // Use smoothed inverse distances as weights.
        weights.set(
                smooth(max(0f, 1f - weights.x)),
                smooth(max(0f, 1f - weights.y)),
                smooth(max(0f, 1f - weights.z))
        );
        // Normalize such that the sum of the weights is one.
        weights.scl(1f / (weights.x + weights.y + weights.z));
        return weights;
    }

    /**
     * The simpler of the two common smooth-step functions.
     *
     * @param x raw value in range {@code [0, 1]}
     * @return the smoothed value in range {@code [0, 1]}
     */
    private static float smooth(float x) {
        return (3f - 2f * x) * x * x;
    }

    public static int cellCount(int subdivisions) {
        // Same as pow(4, SUBDIVISIONS + 1) / 3
        int exp = 2 * subdivisions + 2;
        return (1 << exp) / 3;
    }

    public synchronized static Blueprint get() {
        if (instance == null) {
            instance = new Blueprint();
        }
        return instance;
    }

}
