/*
 * Copyright 2018 Michael Knopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.jimknopf.layercake;

import org.bitbucket.jimknopf.layercake.Blueprint.Cell;

import static org.bitbucket.jimknopf.layercake.Blueprint.N_COORDS;
import static org.bitbucket.jimknopf.layercake.Blueprint.SUBDIVISIONS;

import org.bitbucket.jimknopf.layercake.math.Mat3f;
import org.bitbucket.jimknopf.layercake.math.Vec3f;
import org.bitbucket.jimknopf.layercake.math.Vec3fList;

/**
 * Simple heightmap generator that takes three noise functions as input and
 * outputs a triangular heightmap (in Wavefront Object format).
 */
public class HeightmapGenerator {

    private static final Blueprint BLUEPRINT = Blueprint.get();
    private static final float EPSILON = 0.001f;
    private static final float SAMPLE_HEIGHT = 0.5f;
    private static final float NORMAL_HEIGHT = 5f;

    private static final Mat3f ROTATION_60_DEG = Mat3f.yRotation(-Math.PI / 3d);
    private static final Mat3f ROTATION_M60_DEG = Mat3f.yRotation(Math.PI / 3d);
    private static final Mat3f ROTATION_NONE = Mat3f.identity();

    private static final Vec3f ORIGIN_X, ORIGIN_Y, ORIGIN_Z;

    static {
        Cell root = BLUEPRINT.root();
        Vec3fList positions = BLUEPRINT.positions();
        ORIGIN_X = positions.get(root.vertexX, new Vec3f());
        ORIGIN_Y = positions.get(root.vertexY, new Vec3f());
        ORIGIN_Z = positions.get(root.vertexZ, new Vec3f());
    }

    private final Vec3fList vertices = new Vec3fList(N_COORDS, true);
    private final Vec3fList normals = new Vec3fList(N_COORDS, true);

    private Noise noiseX, noiseY, noiseZ;
    private Mat3f rotation;
    private Mat3f inverseRotation;

    /**
     * Generates a new heightmap in Wavefront Object format using the given
     * noise functions.
     *
     * @param x      the noise function for vertex x
     * @param y      the noise function for vertex y
     * @param z      the noise function for vertex z
     * @param rotate whether to rotate coordinates by 60 degrees
     * @return the heightmap in Wavefront Object format
     */
    public String generate(Noise x, Noise y, Noise z, boolean rotate) {
        this.noiseX = x;
        this.noiseY = y;
        this.noiseZ = z;
        this.rotation = rotate ? ROTATION_60_DEG : ROTATION_NONE;
        this.inverseRotation = rotate ? ROTATION_M60_DEG : ROTATION_NONE;
        sampleVertices();
        return writeWavefront();
    }

    /**
     * Samples all vertices of the blueprint and derives the per vertex normals.
     */
    private void sampleVertices() {
        Vec3fList positions = BLUEPRINT.positions();
        Vec3fList weights = BLUEPRINT.weights();

        Vec3f position = new Vec3f();
        Vec3f weight = new Vec3f();
        Vec3f gradient = new Vec3f();
        Vec3f normal = new Vec3f();

        for (int i = 0; i < positions.size(); i++) {
            normal.set(0f, NORMAL_HEIGHT, 0f);
            position = positions.get(i, position);
            weight = weights.get(i, weight);

            float sample = weight.x
                    * sample(noiseX, ORIGIN_X, position, gradient);
            normal.add(gradient.scl(weight.x));

            sample += weight.y * sample(noiseY, ORIGIN_Y, position, gradient);
            normal.add(gradient.scl(weight.y));

            sample += weight.z * sample(noiseZ, ORIGIN_Z, position, gradient);
            normal.add(gradient.scl(weight.z));

            vertices.set(i, position.x, sample * SAMPLE_HEIGHT, position.z);
            normals.set(i, normal.norm());
        }
    }

    /**
     * Samples the given noise function at the given position and approximates
     * the partial gradient (x- and z- axis only).
     *
     * @param noise    the noise function
     * @param origin   the origin of the noise function in the source
     *                 coordinate system
     * @param position the position to sample
     * @param gradient the partial gradient
     * @return the sample of the given position
     */
    private float sample(Noise noise, Vec3f origin, Vec3f position,
                         Vec3f gradient) {
        // Translate the given position into the coordinate system with the
        // given origin and map from [-1, 1] to [-0.5, 0.5].
        Vec3f vertex = new Vec3f();
        vertex.x = (position.x - origin.x) * 0.5f;
        vertex.z = (position.z - origin.z) * 0.5f;

        // Rotate coordinates around the origin.
        vertex = rotation.mul(vertex, vertex);

        // Translate again to map from [-0.5, 0.5] to [0, 1].
        vertex.x += 0.5f;
        vertex.z += 0.5f;

        // We need three samples to approximate the gradient at the given
        // position (only along the x- and z-axis). EPSILON is added after the
        // rotation to ensure that we always sample the same three points
        // regardless of orientation (we cannot choose an arbitrarily small
        // value for EPSILON due to the limited precision of 32Bit floats).
        float sample = noise.sample(vertex.x, 0f, vertex.z);
        float sampleDx = noise.sample(vertex.x + EPSILON, 0f, vertex.z);
        float sampleDz = noise.sample(vertex.x, 0f, vertex.z + EPSILON);

        gradient.x = (sampleDx - sample) / EPSILON;
        gradient.z = (sampleDz - sample) / EPSILON;

        // Since we did not include EPSILON in the rotation, we need to
        // adjust the orientation of the gradient.
        inverseRotation.mul(gradient, gradient);
        return sample;
    }

    private String writeWavefront() {
        StringBuilder builder = new StringBuilder();
        builder.append("# Heightmap generated with Layercake\n");

        for (Vec3f vertex : vertices) {
            builder.append(String.format("v %.5f %.5f %.5f\n",
                    vertex.x, vertex.y, vertex.z));
        }

        for (Vec3f normal : normals) {
            builder.append(String.format("vn %.5f %.5f %.5f\n",
                    normal.x, normal.y, normal.z));
        }

        int firstLeaf = Blueprint.cellCount(SUBDIVISIONS - 1);
        int totalCells = Blueprint.cellCount(SUBDIVISIONS);
        for (int cellId = firstLeaf; cellId < totalCells; cellId++) {
            Cell cell = BLUEPRINT.cell(cellId);
            int x = cell.vertexX + 1;
            int y = cell.vertexY + 1;
            int z = cell.vertexZ + 1;
            builder.append(String.format("f %d//%d %d//%d %d//%d\n",
                    x, x, y, y, z, z));
        }

        return builder.toString();
    }

}
