/*
 * Copyright 2018 Michael Knopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.jimknopf.layercake;

import org.bitbucket.jimknopf.layercake.math.Vec3f;
import org.bitbucket.jimknopf.layercake.math.Vec3fList;

import java.util.Random;

import static java.lang.Math.*;

/**
 * Simple three-dimensional gradient noise function that maps coordinates from
 * {@code [0, 1]^3} to values in {@code [0, 1]}. Samples are evaluated lazily
 * and are not cached. Thus, the memory usage of this function is very low
 * get the cost of runtime.
 */
public final class Noise {

    private static final float OFFSET_RANGE = 0.25f;
    private static final float UNIT_CUBE_DIAGONAL = (float) sqrt(3);

    private final long seed;
    private final int gridOffsetX, gridOffsetY;
    private final float freqX, freqY, freqZ;

    private final Vec3fList gradients;
    private final float[] offsets;

    /**
     * Creates a new noise function based on the given seed and with the given
     * frequencies.
     *
     * @param seed random generator seed
     * @param fx   frequency along x-axis
     * @param fy   frequency along y-axis
     * @param fz   frequency along z-axis
     */
    public Noise(long seed, float fx, float fy, float fz) {
        this.seed = seed;
        this.freqX = abs(fx);
        this.freqY = abs(fy);
        this.freqZ = abs(fz);

        // Consider the example fx = fy = fz = 1. To sample the point (1, 1, 1)
        // we need to interpolate between the vertices (1, 1, 1), (2, 1, 1),
        // (1, 2, 1), etc. Thus, the grid must be of size 3x3x3.
        int gridX = (int) freqX + 2;
        int gridY = (int) freqY + 2;
        int gridZ = (int) freqZ + 2;

        // Remember offsets instead of height and width of the grid.
        this.gridOffsetX = gridY * gridZ;
        this.gridOffsetY = gridZ;

        // For each vertex, pick a random point on the unit sphere and a random
        // offset (intercept).
        Random rng = new Random(seed);
        int nVertices = gridX * gridY * gridZ;
        this.gradients = new Vec3fList(nVertices);
        this.offsets = new float[nVertices];
        Vec3f gradient = new Vec3f();
        for (int i = 0; i < nVertices; i++) {
            double theta = 2d * PI * rng.nextDouble();
            double z = 2d * rng.nextDouble() - 1d;
            double p = sqrt(1d - z * z);
            gradient.set((float) (p * cos(theta)),
                    (float) (p * sin(theta)),
                    (float) z);
            gradients.add(gradient);
            offsets[i] = rng.nextFloat() * (2f * OFFSET_RANGE) - OFFSET_RANGE;
        }
    }

    /**
     * Samples the noise function at the given point.
     *
     * @param x x-coordinate in range {@code [0, 1]}
     * @param y y-coordinate in range {@code [0, 1]}
     * @param z z-coordinate in range {@code [0, 1]}
     * @return the sampled value in range {@code [0, 1]}
     */
    public float sample(float x, float y, float z) {
        // Ensure bounds and map from [0, 1] to [0, frequency].
        float sx = max(0f, min(x, 1f)) * freqX;
        float sy = max(0f, min(y, 1f)) * freqY;
        float sz = max(0f, min(z, 1f)) * freqZ;

        // Floor input as early as possible to prevent differences due to
        // rounding errors later on.
        int fx = (int) sx;
        int fy = (int) sy;
        int fz = (int) sz;

        // Local coordinates in the surrounding unit cube.
        float px = sx - fx;
        float py = sy - fy;
        float pz = sz - fz;

        // Sample given point for each of the eight closest gradients. Translate
        // point coordinates such that the gradient is always get the origin.
        Vec3f gradient = new Vec3f();
        Vec3f point = new Vec3f();

        // For each edge of the cube parallel to the x-axis, interpolate between
        // samples taken at x = 0 and x = 1.
        float distanceA = smooth(1f - px);
        float distanceB = 1f - distanceA;

        // Interpolate between (0, 0, 0) and (1, 0, 0).
        int index = indexOf(fx, fy, fz);
        gradients.get(index, gradient);
        point.set(px, py, pz);
        float a = gradient.dot(point) + offsets[index];

        index = indexOf(fx + 1, fy, fz);
        gradients.get(index, gradient);
        point.set(px - 1f, py, pz);
        float b = gradient.dot(point) + offsets[index];

        float x00 = distanceA * a + distanceB * b;

        // Interpolate between (0, 1, 0) and (1, 1, 0).
        index = indexOf(fx, fy + 1, fz);
        gradients.get(index, gradient);
        point.set(px, py - 1f, pz);
        a = gradient.dot(point) + offsets[index];

        index = indexOf(fx + 1, fy + 1, fz);
        gradients.get(index, gradient);
        point.set(px - 1f, py - 1f, pz);
        b = gradient.dot(point) + offsets[index];

        float x10 = distanceA * a + distanceB * b;

        // Interpolate between (0, 0, 1) and (1, 0, 1).
        index = indexOf(fx, fy, fz + 1);
        gradients.get(index, gradient);
        point.set(px, py, pz - 1f);
        a = gradient.dot(point) + offsets[index];

        index = indexOf(fx + 1, fy, fz + 1);
        gradients.get(index, gradient);
        point.set(px - 1f, py, pz - 1f);
        b = gradient.dot(point) + offsets[index];

        float x01 = distanceA * a + distanceB * b;

        // Interpolate between (0, 1, 1) and (1, 1, 1).
        index = indexOf(fx, fy + 1, fz + 1);
        gradients.get(index, gradient);
        point.set(px, py - 1f, pz - 1f);
        a = gradient.dot(point) + offsets[index];

        index = indexOf(fx + 1, fy + 1, fz + 1);
        gradients.get(index, gradient);
        point.set(px - 1f, py - 1f, pz - 1f);
        b = gradient.dot(point) + offsets[index];

        float x11 = distanceA * a + distanceB * b;

        // For each face of the cube parallel to the xy-plane, interpolate
        // between the values for its two edges parallel to the x-axis.
        distanceA = smooth(1f - py);
        distanceB = 1f - distanceA;
        float xy0 = distanceA * x00 + distanceB * x10;
        float xy1 = distanceA * x01 + distanceB * x11;

        // Interpolate between the values for the two faces parallel to the
        // xy-axis.
        distanceA = smooth(1f - pz);
        distanceB = 1f - distanceA;
        float xyz = distanceA * xy0 + distanceB * xy1;

        // Normalize the value to [0, 1].
        return xyz / (UNIT_CUBE_DIAGONAL + OFFSET_RANGE) + 0.5f;
    }

    @Override
    public String toString() {
        return String.format("Noise 0x%s (%.3f, %.3f, %.3f)",
                Long.toHexString(seed), freqX, freqY, freqZ);
    }

    /**
     * Returns the index of the nearest gradient that is closer or of equal
     * distance to the origin than the given point.
     *
     * @param x x-coordinate
     * @param y y-coordinate
     * @param z z-coordinate
     * @return index of the nearest gradient
     */
    private int indexOf(int x, int y, int z) {
        return x * gridOffsetX + y * gridOffsetY + z;
    }

    /**
     * The smoother of the two smoothstep functions.
     *
     * @param x value in range {@code [0, 1]}
     * @return the smoothed value in range {@code [0, 1]}
     */
    private static float smooth(float x) {
        return ((6f * x - 15f) * x + 10f) * x * x * x;
    }

}
