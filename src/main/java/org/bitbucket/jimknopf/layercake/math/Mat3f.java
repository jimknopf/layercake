/*
 * Copyright 2018 Michael Knopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.jimknopf.layercake.math;

/**
 * Simple 3 times 3 matrix of {@code float} values.
 */
public class Mat3f {

    private final float[] values;

    /**
     * Creates a new all-zero matrix.
     */
    public Mat3f() {
        values = new float[9];
    }

    /**
     * Creates a new matrix with the given values (ro major).
     *
     * @param values the matrix values
     */
    public Mat3f(float... values) {
        this();
        for (int i = 0; i < 9; i++) {
            this.values[i] = values[i];
        }
    }

    public Vec3f mul(Vec3f vector, Vec3f result) {
        float x = vector.x * values[0] + vector.y * values[1] + vector.z * values[2];
        float y = vector.x * values[3] + vector.y * values[4] + vector.z * values[5];
        float z = vector.x * values[6] + vector.y * values[7] + vector.z * values[8];
        result.set(x, y, z);
        return result;
    }

    public Vec3f mul(float x, float y, float z, Vec3f result) {
        result.x = x * values[0] + y * values[1] + z * values[2];
        result.y = x * values[3] + y * values[4] + z * values[5];
        result.z = x * values[6] + y * values[7] + z * values[8];
        return result;
    }

    @Override
    public String toString() {
        return String.format("((%.3f, %.3f, %.3f), (%.3f, %.3f, %.3f), (%.3f, %.3f, %.3f))",
                values[0], values[1], values[2],
                values[3], values[4], values[5],
                values[6], values[7], values[8]);
    }

    /**
     * Creates a new identity matrix.
     *
     * @return the identity matrix
     */
    public static Mat3f identity() {
        return new Mat3f(
                1f, 0f, 0f,
                0f, 1f, 0f,
                0f, 0f, 1f
        );
    }

    /**
     * Creates a new rotation matrix for rotation around the y-axis.
     *
     * @param alpha rotation in radians
     * @return the rotation matrix
     */
    public static Mat3f yRotation(double alpha) {
        float cos = (float) Math.cos(alpha);
        float sin = (float) Math.sin(alpha);
        return new Mat3f(cos, 0f, sin, 0f, 1f, 0f, -sin, 0f, cos);
    }

}
