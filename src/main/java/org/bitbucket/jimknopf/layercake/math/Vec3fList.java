/*
 * Copyright 2018 Michael Knopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.jimknopf.layercake.math;

import java.nio.FloatBuffer;
import java.util.Arrays;
import java.util.Iterator;

import static java.lang.Math.max;

/**
 * Flat list of {@link Vec3f} vectors backed by a single {@code float} array.
 * The list grows dynamically but never shrinks.
 */
public class Vec3fList implements Iterable<Vec3f> {

    /** Always allocate an array of at least size {@value}. */
    private static final int MIN_CAPACITY = 8;

    private int capacity;
    private int size = 0;

    private float[] data;
    private int offset = 0;

    /**
     * Creates a new empty list with the given capacity.
     *
     * @param initialCapacity the initial capacity
     */
    public Vec3fList(int initialCapacity) {
        this(initialCapacity, false);
    }

    /**
     * Creates a new list with the given capacity. The list is either empty or
     * initialized with zero elements depending on the second parameter.
     *
     * @param initialCapacity the initial capacity
     * @param initialize      whether to initialize the vectors (with zero)
     */
    public Vec3fList(int initialCapacity, boolean initialize) {
        capacity = max(MIN_CAPACITY, initialCapacity);
        data = new float[3 * capacity];
        if (initialize) {
            size = initialCapacity;
            offset = 3 * size;
        }
    }

    public void add(float x, float y, float z) {
        growIfFull();
        data[offset++] = x;
        data[offset++] = y;
        data[offset++] = z;
        size++;
    }

    public void add(Vec3f vector) {
        growIfFull();
        data[offset++] = vector.x;
        data[offset++] = vector.y;
        data[offset++] = vector.z;
        size++;
    }

    public void clear() {
        size = 0;
        offset = 0;
    }

    /**
     * Writes all elements of the list to given buffer.
     *
     * @param buffer the float buffer
     */
    public void write(FloatBuffer buffer) {
        buffer.put(data, 0, offset);
    }

    /**
     * Looks up the ith vector of this list and updates the given vector with
     * its value.
     *
     * @param i      the vector to look up
     * @param vector the vector to update
     * @return the updated vector
     */
    public Vec3f get(int i, Vec3f vector) {
        int indexOffset = 3 * i;
        vector.x = data[indexOffset];
        vector.y = data[indexOffset + 1];
        vector.z = data[indexOffset + 2];
        return vector;
    }

    public void set(int i, float x, float y, float z) {
        int indexOffset = 3 * i;
        data[indexOffset] = x;
        data[indexOffset + 1] = y;
        data[indexOffset + 2] = z;
    }

    public void set(int i, Vec3f vector) {
        int indexOffset = 3 * i;
        data[indexOffset] = vector.x;
        data[indexOffset + 1] = vector.y;
        data[indexOffset + 2] = vector.z;
    }

    public int size() {
        return size;
    }

    @Override
    public Iterator<Vec3f> iterator() {
        return new Iterator<Vec3f>() {

            private final Vec3f vector = new Vec3f();
            private int index = 0;
            private int offset = 0;

            @Override
            public boolean hasNext() {
                return index < size;
            }

            @Override
            public Vec3f next() {
                vector.x = data[offset++];
                vector.y = data[offset++];
                vector.z = data[offset++];
                index++;
                return vector;
            }
        };
    }

    private void growIfFull() {
        if (size == capacity) {
            capacity += capacity >> 1;
            data = Arrays.copyOf(data, 3 * capacity);
        }
    }

}
