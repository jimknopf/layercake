/*
 * Copyright 2018 Michael Knopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.jimknopf.layercake.math;

import static java.lang.Math.sqrt;

/**
 * Mutable vector of three {@code float} values. Methods returning a
 * {@link Vec3f} always return a self-reference (for chaining).
 */
public class Vec3f {

    public float x, y, z;

    public Vec3f() {
        this.x = 0f;
        this.y = 0f;
        this.z = 0f;
    }

    public Vec3f(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vec3f(Vec3f other) {
        this.x = other.x;
        this.y = other.y;
        this.z = other.z;
    }

    public Vec3f set(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
        return this;
    }

    public Vec3f set(Vec3f other) {
        this.x = other.x;
        this.y = other.y;
        this.z = other.z;
        return this;
    }

    public Vec3f add(Vec3f other) {
        x += other.x;
        y += other.y;
        z += other.z;
        return this;
    }

    public Vec3f sub(Vec3f other) {
        x -= other.x;
        y -= other.y;
        z -= other.z;
        return this;
    }

    public Vec3f scl(float scalar) {
        x *= scalar;
        y *= scalar;
        z *= scalar;
        return this;
    }

    public Vec3f cross(Vec3f other) {
        float tmpX = y * other.z - z * other.y;
        float tmpY = z * other.x - x * other.z;
        float tmpZ = x * other.y - y * other.x;
        x = tmpX;
        y = tmpY;
        z = tmpZ;
        return this;
    }

    public float dot(Vec3f other) {
        return x * other.x + y * other.y + z * other.z;
    }

    public float len() {
        return (float) sqrt(x * x + y * y + z * z);
    }

    public Vec3f norm() {
        float length = len();
        if (length > 0f) {
            x /= length;
            y /= length;
            z /= length;
        }
        return this;
    }

    @Override
    public String toString() {
        return String.format("(%.3f, %.3f, %.3f)", x, y, z);
    }

}
