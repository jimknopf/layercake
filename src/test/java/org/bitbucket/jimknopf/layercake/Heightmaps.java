/*
 * Copyright 2018 Michael Knopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.jimknopf.layercake;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Example generating two triangular heightmaps (a, b, c) and (c, b, d) that
 * together form a seamless prism (one of the two triangles needs to be
 * rotated by 60 degrees).
 */
public class Heightmaps {

    public static void main(String[] args) throws IOException {
        HeightmapGenerator generator = new HeightmapGenerator();

        Noise a = new Noise(11294346L, 5f, 0f, 6f);
        Noise b = new Noise(8333436L, 2f, 0f, 4f);
        Noise c = new Noise(931946L, 3f, 0f, 7f);
        Noise d = new Noise(2931946L, 2f, 0f, 4f);

        String object = generator.generate(a, b, c, false);
        Files.write(Paths.get("heightmap_abc.obj"),
                object.getBytes(StandardCharsets.UTF_8));

        object = generator.generate(c, b, d, true);
        Files.write(Paths.get("heightmap_cbd.obj"),
                object.getBytes(StandardCharsets.UTF_8));
    }

}
