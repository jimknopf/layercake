/*
 * Copyright 2018 Michael Knopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.jimknopf.layercake;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

import static java.awt.image.BufferedImage.TYPE_BYTE_GRAY;

import java.io.File;
import java.io.IOException;

/**
 * Example generating a 2D random cloud texture.
 */
public class NoiseTexture {

    /** The image size */
    private static final int SIZE = 512;

    public static void main(String[] args) throws IOException {
        Noise noise = new Noise(577413L, 3f, 3f, 0f);

        BufferedImage image = new BufferedImage(SIZE, SIZE, TYPE_BYTE_GRAY);
        for (int x = 0; x < SIZE; x++) {
            float px = (float) x / SIZE;
            for (int y = 0; y < SIZE; y++) {
                float sample = noise.sample(px, (float) y / SIZE, 0f);
                // Create RGB pixel with with identical color components.
                int intensity = (int) (255f * sample);
                int pixel = intensity | (intensity << 8) | (intensity << 16);
                image.setRGB(x, y, pixel);
            }
        }

        File out = new File("texture.png");
        ImageIO.write(image, "png", out);
    }

}
