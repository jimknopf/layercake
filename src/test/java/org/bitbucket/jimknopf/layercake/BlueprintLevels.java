/*
 * Copyright 2018 Michael Knopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.jimknopf.layercake;

import org.bitbucket.jimknopf.layercake.Blueprint.Cell;
import org.bitbucket.jimknopf.layercake.math.Vec3f;
import org.bitbucket.jimknopf.layercake.math.Vec3fList;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * Example iterating over the cells of the {@link Blueprint} level by level
 * writing each cell to a Wavefront Object file.
 */
public class BlueprintLevels {

    private final static Blueprint BLUEPRINT = Blueprint.get();
    private final static float LAYER_HEIGHT = 0.1f;

    private static void layersToMesh(Writer writer) throws IOException {
        Vec3fList positions = BLUEPRINT.positions();
        Vec3f position = new Vec3f();
        for (int level = 0; level <= Blueprint.SUBDIVISIONS; level++) {
            float height = level * LAYER_HEIGHT;
            int firstOnLevel = Blueprint.cellCount(level - 1);
            int firstOnNextLevel = Blueprint.cellCount(level);
            for (int id = firstOnLevel; id < firstOnNextLevel; id++) {
                Cell cell = BLUEPRINT.cell(id);

                position = positions.get(cell.vertexX, position);
                writer.write(String.format("v %.5f %.5f %.5f\n",
                        position.x, height, position.z));

                position = positions.get(cell.vertexY, position);
                writer.write(String.format("v %.5f %.5f %.5f\n",
                        position.x, height, position.z));

                position = positions.get(cell.vertexZ, position);
                writer.write(String.format("v %.5f %.5f %.5f\n",
                        position.x, height, position.z));

                // Connect the last three written vertices (in order x, y, z)
                writer.write("f -3 -2 -1\n");
            }
        }
    }

    public static void main(String[] args) throws IOException {
        try (Writer writer = Files.newBufferedWriter(Paths.get("levels.obj"),
                StandardOpenOption.CREATE, StandardOpenOption.WRITE)) {
            layersToMesh(writer);
        }
    }

}
